#include <stdio.h>
extern int yylex();
extern int yylineno;
extern char* yytext;
extern FILE* yyin;
extern int yyleng;

int menu()
{
  int opc;
  while(opc<1 || opc>2){
    printf("\t%s\n","Seleccione una opcion");
    printf("\n\t%s\n","1- Analizar Codigo(analizador lexico)");
    printf("\n\t%s\n","2-Salir de la aplicacion");
    printf("\n\t%s ","Seleccione una opcion");
    scanf("%d",&opc);
  }
  return opc;
}

int analizadorLexico()
{
  int ntoken, contador, ColIn,ColFin,acumulador;
  FILE *arch;
  char tipo[12][50] = {"PalabraReservada","ID","Entero","Operador Aritmetic","Oper Inc Dec","Op Relacional","Op Logico","Op Combinado","Otro","comenatario","Caracter especial","No identificado"};
  printf("\tAnalizador lexico de C\n");
  yyin = fopen("ejecutable.txt","r");
  arch = fopen("archivo","w");
  ntoken = yylex();
  fprintf(arch,"%10s %30s %20s %13s\n","Lexema","Token","Linea","Columna");
  printf("%10s %30s %20s %13s\n","Lexema","Token","Linea","Columna");

  ColIn = 0;
  ColFin = 0;
  acumulador = 0;
  while(ntoken>0)
  {
    if(ntoken == 13)
    {
      ColIn = ColFin;
      ColFin = ColFin + yyleng;
      printf("%10s %30s %20d %5d %5s %5d\n"," ",tipo[10],yylineno,ColIn-acumulador,"-",ColFin-acumulador-1);
      fprintf(arch,"%10s %30s %20d %5d %5s %5d\n"," ",tipo[10],yylineno,ColIn-acumulador,"-",ColFin-acumulador-1);
      acumulador = ColFin;
      ntoken = yylex();
    }else{
      ColIn = ColFin;
      ColFin = ColFin + yyleng;
      printf("%10s %30s %20d %5d %5s %5d\n",yytext,tipo[ntoken-1],yylineno,ColIn-acumulador,"-",ColFin-acumulador-1);
      fprintf(arch,"%10s %30s %20d %5d %5s %5d\n",yytext,tipo[ntoken-1],yylineno,ColIn-acumulador,"-",ColFin-acumulador-1);
      ntoken = yylex();
    }
  }
  printf("%s\n\n","");
  fclose(yyin);
  fclose(arch);
}

int main()
{
  int opc = -1;
  while(opc != 2){
    printf("%30s\n\n", "Analizador Lexico de C");
    opc = menu();

    switch (opc) {
      case 1:
        analizadorLexico();
        break;
      case 2:
        break;
    }
  }
  return 0;
}
