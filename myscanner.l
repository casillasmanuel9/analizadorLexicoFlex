%option yylineno

%{
#include "myscanner.h"
%}

ID  [a-z]([a-z]|[A-Z]|[0-9])*
palRes (i(nt|f)|else|return|void|while)
entero  [0-9][0-9]*
operArit  (\+|\-|\*|\/|=)
operIncDec  (\+\+|\-\-)
opeRel  (\<(=?)|\>(=?))|(=|!)=
opeLog (\&\&|\!|\|\|)
opeCom  (\+|\-|\*|\/)=
otro  (;|\.|\(|\)|\{|\}|\[|\])
comment (\/((\*+(.|\n)*\*\/)|(\/.*)))
carSpec (" "|\t)
not_ident .

%%
{palRes}  return palabraReservada;
{ID}  return ID;
{entero} return integer;
{operArit} return operArit;
{operIncDec}  return operIncDec;
{opeRel}  return opeRel;
{opeLog} return opeLog;
{opeCom}  return opeCom;
{otro}  return otro;
{comment} return comment;
{carSpec} return carSpec;
{not_ident}  return not_ident;
\n  {return 13;}
%%

yywrap() {
  return 1;
}
